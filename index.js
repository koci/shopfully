const dotenv = require("dotenv");
const express = require("express");
const utils = require("./app/utils/utils.js");
const path = require("path");

/* IMPORT ROUTE */
const flyersRoute = require("./app/routes/flyers.route.js");

/* INIT */
dotenv.config();
const app = express();

/* MIDDLEWARE */
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");
  res.setHeader("Access-Control-Allow-Methods", "GET");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

/* VIEWS ENGINE */
app.set("views", path.join(__dirname, "app/views"));
app.use(express.static('app/public'));

app.set("view engine", "ejs");

/* ROUTING */
var router = express.Router();
app.use("/api", router);
flyersRoute(router);

// respond with "hello world" when a GET request is made to the homepage
app.get("/", function(req, res) {
  res.render("index");
});

/* SERVER */
const PORT = process.env.PORT;
app.listen(PORT, function() {
  console.info(`Server is listening on http://localhost:${PORT}`);
});
