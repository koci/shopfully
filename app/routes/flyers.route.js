const flyersAPI = require("./api/flyers.api.js");

const flyersRouter = function(router) {
  router.route("/flyers")
  .get(flyersAPI.findAll);
};

module.exports = flyersRouter;
