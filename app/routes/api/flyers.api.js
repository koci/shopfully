const flyersController = require("../../controllers/flyers.controller.js");

exports.findAll = function(req, res) {
  const { page = 1, limit, expired = false } = req.query;

  flyersController
    .findAll({ page, limit, expired })
    .then(flyers => {
      const results = {
        code: 200,
        data: flyers
      };
      res.send(results);
    })
    .catch(err => {
      console.error(err);
      res.status(500).send({
        code: 500,
        message: "Some error occurred while retrieving flyers."
      });
    });
};
