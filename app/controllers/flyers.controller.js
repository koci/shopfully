var csvparse = require("./libs/csvparse");
const assert = require("assert");

const PAGE_FLYERS_LIMIT = 100;

/**
 * Returns an array of flyers, based on the option passed as an argument
 * @param {Object} options {'limit', 'page'} options for limit number of flyers result and page value
 * @returns {Array} list of flyers
 */
exports.findAll = async function(options) {
  options.limit = parseInt(options.limit ? options.limit : PAGE_FLYERS_LIMIT);
  options.page = parseInt(options.page - 1);
  options.expired = options.expired == true  || options.expired == 'true';
  
  assert(typeof options.limit == "number" && !isNaN(parseInt(options.limit)));
  assert(typeof options.page == "number" && !isNaN(parseInt(options.page)));
  assert(typeof options.expired == "boolean");

  const path = process.env.DB;
  const from = options.page * PAGE_FLYERS_LIMIT;
  const to =
    options.limit < PAGE_FLYERS_LIMIT
      ? from + options.limit
      : from + PAGE_FLYERS_LIMIT;

  function filterItem(item) {
    const isPublished =  item.is_published == 1;
    const isPastDate =  options.page ? true : new Date() < new Date(item.end_date).withoutTime();
    return isPublished && isPastDate;
  }

 
  const flyers = await csvparse.readCsvLines(path, from, to, filterItem);
    
  return flyers;
};
