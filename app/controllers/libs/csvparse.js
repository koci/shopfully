const csv = require("csv-parser");
const fs = require("fs");

/**
 * Read a csv line based on from and to params and filterItem
 * @param {string} path path of csv file
 * @param {int} from index of the first line to read
 * @param {int} to index of the lasst line to read
 * @param {function} filterItem function that return true or false used to filter out the flyers
 * @returns {Array} Lines of csv file, structured according to the name of the columns
 */
exports.readCsvLines = function(path, from, to, filterItem) {
  var buffer = "",
    items = [],
    j = 0;

  return new Promise((resolve, reject) => {
    fs.createReadStream(path)
      .pipe(csv())
      .on("data", item => {
        if (items.length < to - from) {
          const valid = filterItem(item);
          if (valid) {
            j++;
            if (j >= from) {
              items.push(item);
            }
          }
        }
      })
      .on("end", () => {
        resolve(items);
      });
  });
};
