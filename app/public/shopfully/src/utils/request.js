import axios from 'axios'

const BASE_URL = window.location.hostname == 'localhost' ? 'http://localhost:50607' : 'http://localhost:50607';

const service = axios.create({
  baseURL: BASE_URL, 
  timeout: 100000 
})

export default service
