import Vue from 'vue'

var state = {
  flyers: [],
  favouritesFlyers:[]
}

const getters = {
  getFlyers: state => state.flyers,
  getFavouritesFlyers: state => state.favouritesFlyers
}

const mutations = {
  ADD_FLYERS: (state, newFlyers) => {
    newFlyers.forEach((newFlyer,i) => {
      newFlyer._favorite = state.favouritesFlyers[i] != undefined;
      state.flyers.push(newFlyer);
    });
  },
  DELETE_FLYERS: (state) => {
    state.flyers = []
  },
  ADD_FAVORITE_FLYER: (state, flyerIndex) => {
   const exist = state.favouritesFlyers.find((i) => i.id == state.flyers[flyerIndex].id) != undefined;
   if(!exist){
    state.flyers[flyerIndex]._favorite = true;
     state.favouritesFlyers.push( state.flyers[flyerIndex]);
   }
  },
  DELETE_FAVORITE_FLYER: (state, flyerIndex) => {
    //trova flyer con index == index e eliminalo
    const favFlyerIndex = state.favouritesFlyers.findIndex((i) => i.id == state.flyers[flyerIndex].id);
    if(favFlyerIndex != undefined){
      state.favouritesFlyers.splice(favFlyerIndex,1);
      state.flyers[flyerIndex]._favorite = false;
    }
  }
};


const actions = {

  addFlyers: (context, payload) => {
    context.commit("ADD_FLYERS", payload)
  },
  deleteFlyers: (context) => {
    context.commit("DELETE_FLYERS")
  },
  addFavouriteFlyer: (context, payload) => {
    context.commit("ADD_FAVORITE_FLYER", payload)
  },
  deleteFavouriteFlyer: (context, payload) => {
    context.commit("DELETE_FAVORITE_FLYER", payload)
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}
