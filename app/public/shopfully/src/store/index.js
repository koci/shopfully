export const strict = false
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persistedstate'
import flyer from './modules/flyer'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    flyer
  },
  plugins: [VuexPersistence()]
})

export default store;
