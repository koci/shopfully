import request from '../utils/request'


export function getFlyers(page,limit) {
    return request({
      url:`/api/flyers?page=${page}&limit=${limit}&expired=false`,
      method: 'get'
    })
  }
