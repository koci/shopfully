# ShopFully Backend

## Sviluppo
La parte backend utilizza Node.js basandosi su express per la gestione del server, la logica per la lettura del database in formato csv, è in gran parte contenuta nel file app/controllers/libs/csvparse.js, per comodità è stata utilizzata una libreria per la lettura del csv, senza dover fare lo split(',') ad ogni riga dell file csv letto tramite stream.

##Lettura csv e filtri
Come da specifiche il file csv viene letto come stream, leggendo ogni linea del file csv, 
tramite una funzione passata a readCsvLines(path, from, to, filterItem) ( /app/controllers/libs/csvparse.js)
vengono filtrati gli elementi in base al campo "is_published" e alla data di scadenza del volantino, in modo da restituire sempre un numero di flyers uguali a quelli richiesti dal client attraverso la query "limit="

##Query String e limiti
Le query string accettate sono "page", "limit", "expired", se la pagina non è impostata, viene presa come default la pagina "1", se il limite invece non viene settato, viene preso il limite massimo ovvero "100", infine "expired" è un booleano che permette di scegliere se mostrare i flyers con data di scadenza passata o meno.


## Struttura cartelle app
Il file principale del server è index.js è contiene le dipendenze e la creazione del server.

- controllers: contiene sia il file csvparse per la lettura del csv, che flyers.controller che ne gestisce il funzionamento
- db: contiene il file csv
- public: parte frontend del progetto
- routes: routes api del server 
- utils: funzioni ausiliarie e di supporto 
- views: contiene i file statici serviti dal server nodejs ( express )


## Avviare il server

- npm install / yarn install
- node index.js


# ShopFully Frontend

La parte client è stata realizzata con il framework Vuejs, purtoppo per motivi di tempo e lavoro non è stata ottimizzata al meglio, soprattuto nella fase di deploy e pacchettizzazione del build finale, dove manca l'automastimo per far coesistere le view di express con la generazione automatica della build di vue.


##Salvataggio dei preferiti

La parte della gestione dei preferiti viene gestita con Vuex ( simile alla controparte Redux di react) attraverso uno State Management ed una libreria per memorizzarli su localstorage ( Vuex-persistedstate https://github.com/robinvdvleuten/vuex-persistedstate) i preferiti saranno temporaneamente disponibili sul client senza dover memorizzare nulla lato server.

## Avvio (npm o yarn)

- yarn install
- yarn serve

## Compilazione 
attraverso il comando "yarn build" verrà creata la cartella "dist" contenente il build dell'applicazione frontend, da servire con "express" (nodejs), per un test veloce l'output della build è stata copiata a mano nella cartella app/public/ per essere disponibile al server nodejs



